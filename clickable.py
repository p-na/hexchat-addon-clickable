"""
clickable.py - The convenience tool

clickable.py is ment to replace text with something more obvious and it's
particularly made for, but not limited to, replacing any kind of IDs with their
corresponding URLs.  

It won't replace anything in-place, the replacement is shown either privately
or posted to the channel.

.. note::

    Please note that `sender` and `msg` are reserved keywords and will always replace named matches
    with the same name.  They may be used in the `repl`(acement) string.

"""
__module_name__ = "clickable"
__module_version__ = "0.1.0"
__module_description__ = "enables you to decode IDs to URLs"

import hexchat
import re
import sys

POST_TO_CHANNEL = False
# USE_COLORS = True  # http://hexchat.readthedocs.io/en/latest/script_python.html
CONFIG = [
        # {
        #     're': r'custom-id-(?P<custom_id>\d+)',
        #     'repl': 'Custom ID: {custom_id} from {sender}'
        # },
        {
            're': r'pr\s*#(?P<id>\d+)',
            'repl': 'https://github.com/ceph/ceph/pull/{id}',
            'recipient': ['#ceph-dashboard', '#ceph'],
        }, {
            're': r'pr\s*#(?P<id>\d+)',
            'repl': 'https://bitbucket.org/openattic/openattic/pull-request/{id}',
            'recipient': '#openattic',
        },
]


def privmsg_cb(word, word_eol, userdata):
    import requests
    sender = word[0].split('@')[0].split(':')[1].split('!')[0]
    recipient = word[2]
    msg = ' '.join(word[3:]).lstrip(':')

    hits = []

    for entry in CONFIG:
        if 'recipient' in entry:
            if isinstance(entry['recipient'], list):
                if not recipient in entry['recipient']:
                    continue
            else:
                if recipient != entry['recipient']:
                    continue

        match = re.search(entry['re'], msg, flags=re.IGNORECASE)
        if match:
            if 'excludePattern' in entry:
                excludePattern = re.search(entry['excludePattern'], msg, flags=re.IGNORECASE)
                if excludePattern:
                    continue

            kwargs = match.groupdict()
            kwargs.update(sender=sender, msg=msg)
            replacement = entry['repl'].format(*match.groups(), **kwargs)

            hits.append(replacement)

    message = ' '.join(hits)

    if hits:
        if POST_TO_CHANNEL:
            hexchat.command('MSG {target} {message}'.format(target=recipient,
                message=message))
        else:
            print(message)

    return hexchat.EAT_NONE

def unload_cb(userdata):
    hexchat.unhook(handler)

handler = hexchat.hook_server('PRIVMSG', privmsg_cb)
hexchat.hook_unload(unload_cb)
